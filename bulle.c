/**
 * @file 
 * @author Thibault LOUAPRE, Geoffrey SAUVAGEOT
 * @version 1.0
 * @brief Fichier des fonctions de tri par bulle
 * @date 26-09-2019
 */

#include <stdio.h>
#include <stdlib.h>

/** 
 * @brief Trie un tableau par ordre croissant (par méthode tri bulle)
 * @param float tab[] : tableau à trier
 * @param int size : taille du tableau
 * @return tab[] la tableau trié
 * @since Version 1.0 
 */
float* triBulleAsc(float tab[], int size) 
{
    for (int i=0; i<size; i++) 
    {
        for (int j=0; j<size-1; j++) 
        {
            if(tab[j] > tab[j+1]) 
            {
                float tmp = tab[j+1];
                tab[j+1] = tab[j];
                tab[j] = tmp;
            }
        }
        
    }
    
    return tab;
}

/** 
 * @brief Trie un tableau par ordre décroissant (par méthode tri bulle)
 * @param float tab[] : tableau à trier
 * @param int size : taille du tableau
 * @return tab[] la tableau trié 
 * @since Version 1.0 
 */
float* triBulleDesc(float tab[], int size) 
{
    for (int i=0; i<size-1; i++) 
    {
        for (int j=size-1; j>0; j--) 
        {
            if(tab[j-1] < tab[j]) 
            {
                float tmp = tab[j];
                tab[j] = tab[j-1];
                tab[j-1] = tmp;
            }
        }
    }
    
    return tab;
}

