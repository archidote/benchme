# Benchme

Projet d'implémentation de trois algorithmes de tri : selection, bulle, insertion. 

## Pour commencer

Récupérer notre dépot : 

Veuillez installer git sur votre terminal avant d'éxécuter les commandes suivante : 

```
git clone 

https://gitlab.com/brlndtech/benchme

````

## Fichiers

Voici la liste des fichiers du projets 

- main.c
- bulle.c
- insertion.c 
- selection.c
- comparaisonTri.c
- programme main.c
- header.h

## Installation 

Importer le projet avec votre IDE préféré. (Il doit supporter le langage C)



## éxécution du programme 


#### éxécuter via un IDE 

Importer l'ensemble du projet benchme sur votre IDE (pouvant interpréter le C), et éxécuter le programme.

-> le fichier .csv se situera à la racine du projet benchme.


## Documentation technique du code 

La documentation technique généré via doxygene (format javadoc) est disponnible dans le dossier <code>html</code> à la racine du projet benchme

la commande <code> make documentation </code> fonctionne également, pour afficher directement la page au format .html

(attention cette commande est à éxécuter à l'intérieur du dossier du projet benchme)

## Versions

**Dernière version stable : Master** <br>
**Dernière version : Release** 

## Auteurs


**Sauvageot-Berland Geoffrey** <br>
**Louapre Thibault** 

## License

Ce projet est sous licence ``GPL``


