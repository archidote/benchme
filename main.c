/**
 * @file 
 * @author Thibault LOUAPRE, Geoffrey SAUVAGEOT
 * @version 1.0
 * @brief Fichier Main, tests des fonctions de tri
 * @date 26-09-2019
 */
#include <stdio.h>
#include <stdlib.h>

#include "header.h"

int main(int argc, char** argv) {
    
    float tableau[5] = { 74.25, 82.42, 26.57, 6.23, 19.32 };

     
    printf("\nTri à bulle croissant : \n");
    
    float* testBulleAsc = triBulleAsc(tableau, 5);

    for (int i=0; i < 5; i++) {
        printf("%f\n", testBulleAsc[i]);
    }
    
    printf("\nTri à bulle décroissant : \n");
    
    float* testBulleDesc = triBulleDesc(tableau, 5);

    for (int i=0; i < 5; i++) {
        printf("%f\n", testBulleDesc[i]);
    }
    
    printf("\nTri sélection croissant : \n");

    float* testAsc = triSelectionAsc(tableau, 5);

    for (int i=0; i < 5; i++) {
        printf("%f\n", testAsc[i]);
    }
    
    printf("\nTri sélection décroissant : \n");
    
    float* testDesc = triSelectionDesc(tableau, 5);

    for (int i=0; i < 5; i++) {
        printf("%f\n", testDesc[i]);
    }
    
    printf("\nTri insertion croissant : \n");
    
    float* testInsertionAsc = triInsertionAsc(tableau, 5);

    for (int i=0; i < 5; i++) {
        printf("%f\n", testInsertionAsc[i]);
    }
    
    printf("\nTri insertion décroissant : \n");

    float* testInsertionDesc = triInsertionDesc(tableau, 5);

    for (int i=0; i < 5; i++) {
        printf("%f\n", testInsertionDesc[i]);
    }
   
    
    printf("\nBenchmark des algorithmes de tri : ");
    
    benchmarkTris();
    
    
    void tableauAleatoire(float val[],int lenght);

    return (EXIT_SUCCESS);
}